﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Torre.Models
{


    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Aggregators
    {
    }

    public class Organization
    {
        public int id { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
    }

    public class Data
    {
        public string code { get; set; }
        public string currency { get; set; }
        public double minAmount { get; set; }
        public double maxAmount { get; set; }
        public string periodicity { get; set; }
    }

    public class Compensation
    {
        public Data data { get; set; }
        public bool visible { get; set; }
    }

    public class Skill
    {
        public string name { get; set; }
        public string experience { get; set; }
    }

    public class Member
    {
        public string subjectId { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string professionalHeadline { get; set; }
        public string picture { get; set; }
        public bool member { get; set; }
        public bool manager { get; set; }
        public bool poster { get; set; }
        public double weight { get; set; }
    }

    public class Question
    {
        public string id { get; set; }
        public string text { get; set; }
        public DateTime date { get; set; }
    }

    public class Context
    {
        public List<object> signaled { get; set; }
    }

    public class Opportunity
    {
        public double completion { get; set; }
    }

    public class Input
    {
        public object criteria { get; set; }
        public Opportunity opportunity { get; set; }
    }

    public class And
    {
        [JsonProperty("@type")]
        public string Type { get; set; }
        public string id { get; set; }
        public Input input { get; set; }
        public double score { get; set; }
    }

    public class Scorer
    {
        [JsonProperty("@type")]
        public string Type { get; set; }
        public double score { get; set; }
        public List<And> and { get; set; }
    }

    public class Meta
    {
        public double rank { get; set; }
        public Scorer scorer { get; set; }
        public object filter { get; set; }
        public List<string> boosters { get; set; }
    }

    public class Result
    {
        public string id { get; set; }
        public string objective { get; set; }
        public string type { get; set; }
        public List<Organization> organizations { get; set; }
        public List<string> locations { get; set; }
        public bool remote { get; set; }
        public bool external { get; set; }
        public DateTime? deadline { get; set; }
        public string status { get; set; }
        public Compensation compensation { get; set; }
        public List<Skill> skills { get; set; }
        public List<Member> members { get; set; }
        public List<Question> questions { get; set; }
        public Context context { get; set; }
        public Meta _meta { get; set; }
    }

    public class Root
    {
        public Aggregators aggregators { get; set; }
        public int offset { get; set; }
        public List<Result> results { get; set; }
        public int size { get; set; }
        public int total { get; set; }
    }


}

