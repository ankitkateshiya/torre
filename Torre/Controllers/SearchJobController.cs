﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Torre.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;

namespace Torre.Controllers
{
    public class SearchJobController : Controller
    {
        // GET: SearchJob
      
            string Baseurl = "https://search.torre.co/";
        //string Baseurl = "https://bio.torre.co/";
       
        public async Task<ActionResult> Index()
            {
                List<Root> EmpInfo = new List<Root>();
//---------------
            var request = new HttpRequestMessage(HttpMethod.Post, "https://search.torre.co/opportunities/_search/?");
            //  request.Version = new Version(2, 0);

            var handler = new HttpClientHandler();
            using (var client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
                client.DefaultRequestHeaders.Connection.Add("keep-alive");

                using (HttpResponseMessage response = await client.SendAsync(request))
                {
                    var EmpResponse1 = response.Content.ReadAsStringAsync().Result;

                  
                    //    //Deserializing the response recieved from web api and storing into the Employee list  
                      EmpInfo = JsonConvert.DeserializeObject<List<Root>>("["+EmpResponse1+"]");

                }
            }
            //-------------

            return View(EmpInfo[0].results);
        }
    }

//    #region MyRegion
//         using (var client = new HttpClient())
//                {
//                    //Passing service base url  
//                    client.BaseAddress = new Uri(Baseurl);
//    SearchJob searchJob = new SearchJob();
//    client.DefaultRequestHeaders.Clear();
//                var content = new FormUrlEncodedContent(new[]
//                 {
//                      new KeyValuePair<string, string>("", "")
//                 });
//    //Define request data format  
//    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
//                client.DefaultRequestHeaders.Connection.Add("keep-alive") ;
               
            
//                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
//                HttpResponseMessage Res = await client.PostAsync("opportunities/_search/?", content);
//                    //HttpResponseMessage Res = await client.GetAsync("api/bios/ankitkateshiya");

//                    //Checking the response is successful or not which is sent using HttpClient  
//                    if (Res.IsSuccessStatusCode)
//                    {
//                        //Storing the response details recieved from web api   
//                        var EmpResponse = Res.Content.ReadAsStringAsync().Result;

//    //Deserializing the response recieved from web api and storing into the Employee list  
//    EmpInfo = JsonConvert.DeserializeObject<List<SearchJob>>(EmpResponse);

//                    }
////returning the employee list to view  
//return View(EmpInfo);
//                }
//    #endregion

}